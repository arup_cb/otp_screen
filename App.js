import React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import AuthStack from './components/AuthStack';
import HomeStack from './components/HomeStack';
import { useEffect,useState } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
// import ProfileDrawer from './components/ProfileDrawer';

const App = () => {
  const STORAGE_KEY = '@userOtp';
  const [userActive, setUserActive] = useState(false);
  
  useEffect(() => {
    readData();
  }, [])

  const readData = async () =>{
    try{
      const fetchCode = await AsyncStorage.getItem(STORAGE_KEY)
      if(fetchCode !== null){
        console.log(fetchCode);
        setUserActive(true);
      }
    }catch(e){
      alert('Failed to fetch data from storage');
    }
  }
  
  return (
    <NavigationContainer>
      {/* <ProfileDrawer /> */}
      {userActive ? <HomeStack /> : <AuthStack />}  
    </NavigationContainer>
  );
};

export default App;
