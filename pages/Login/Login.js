import React from 'react';
import {Text,View,TouchableHighlight,StyleSheet} from 'react-native';

 function Login({navigation}) {
    const loginFunc = () => {
        navigation.navigate('Home');
        console.log('hello');
    }
    return (
        <View style={styles.container}>
            <View style={styles.theader}>
                <Text style={styles.title}>Login Page</Text>
            </View>            
            <View style={styles.btn_box}>
                <TouchableHighlight 
                    onPress={()=>loginFunc()} 
                    activeOpacity={0.6}
                    underlayColor="grey">
                    <View style={styles.btn}>
                        <Text style={styles.tbtn}>Login with OTP</Text>
                    </View>                    
                </TouchableHighlight>
            </View>            
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        backgroundColor:'grey',
        flex:1,
    },
    theader:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
    },
    title:{
        fontSize:30,
        color:'black',
    },
    btn_box:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
    },
    btn:{
        backgroundColor:'black',
        borderRadius:5,
        padding:10,
    },
    tbtn:{
        color:'white',
    }
})
export default Login;
   