import React, { useState, useEffect } from 'react';
import { View,StyleSheet} from "react-native";
import VerifyButton from '../../components/VerifyButton';
import EnterField from '../../components/EnterField';
import TrivialMessage from '../../components/TrivialMessage';
import AsyncStorage from '@react-native-community/async-storage';

function Home({navigation}) {
  const STORAGE_KEY = '@userOtp';
  const [userValidData, setUserValidData] = useState(null);
  const [password, setPassword] = useState('token');
 
  //  useEffect(()=>{
  //   readData();
  // },[]);

  const saveData = async () =>{
    try{
      await AsyncStorage.setItem(STORAGE_KEY, password)
      alert('Data Successfully saved');
      console.log(password);
    } catch(e){
      alert('Failed to Save data to the storage');
    }
  }

  const goToNextPageFunc = () =>{
    const originalOtp = [1,2,3,4];
    if(userValidData !== null){
    let result = userValidData.map(item=>{
      return parseInt(item)
    });
    let modifiedData = JSON.stringify(result.sort());
    let isEqual = (JSON.stringify(originalOtp.sort()) === (modifiedData));
      if(isEqual){
        navigation.navigate("MyAccount");
        saveData();
      }else{
        alert('Please enter correct OTP');
      }
  }
    else{
      alert('No OTP given');
    }
  }

    return (
        <View style = {styles.container}>
          <TrivialMessage />
          <EnterField maxCount={4} authUserData={setUserValidData}/>
          <VerifyButton onConfirm={goToNextPageFunc} />            
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
      flex:5,
    }
  });

export default Home;

