import React from 'react';
import {Text,View,TouchableHighlight,StyleSheet} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';

 function SignUp({navigation}) {
    const signUpFunc = () => {
        navigation.navigate('Login');
    }
    return (
        <View style={styles.container}>
            <View style={styles.theader}>
                <Text style={styles.title}>Sign Up Page</Text>
            </View>
            <View style={styles.signUpBox}>
                <View style={styles.labelField}>
                    <Text style={styles.titleName}>Email:</Text>
                    <Text style={styles.titlePass}>Password:</Text>
                </View>
                <View style={styles.inputField}>
                    <TextInput 
                        placeholder="Enter email here..."
                        style={styles.nameData}/>
                    <TextInput 
                        secureTextEntry={true}
                        placeholder="Enter password here..."
                        style={styles.passData}/>
                </View>
            </View>            
            <View style={styles.btn_box}>
                <TouchableHighlight 
                    onPress={()=>signUpFunc()}>
                    <View style={styles.sign_btn}>
                        <Text>Sign Up</Text>
                    </View>                    
                </TouchableHighlight>
                <TouchableHighlight 
                    onPress={()=>signUpFunc()} >
                    <View style={styles.log_btn}>
                        <Text>Go To Login</Text>
                    </View>                    
                </TouchableHighlight>
            </View>            
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        backgroundColor:'salmon',
        flex:1,
    },
    theader:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
    },
    title:{
        fontSize:30,
        color:'white',
    },
    btn_box:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'row',
    },
    sign_btn:{
        backgroundColor:'white',
        borderRadius:5,
        padding:10,
        margin:5,
    },
    log_btn:{
      backgroundColor:'white',
      borderRadius:5,
      padding:10,
      margin:5,
  },
    signUpBox:{
      flexDirection:'row',
      margin:10,
    },
    titleName:{
        fontSize:20,
        color:'white',
    },
    titlePass:{
        fontSize:20,
        color:'white',
    },
    labelField:{
      justifyContent:'space-evenly',
      alignItems:'center',
      width:'30%',
  },
    inputField:{
    justifyContent:'center',
    alignItems:'center',
    width:'70%',
  },
  nameData:{
    width:'100%',
    marginBottom:5,
    borderRadius:4,
    backgroundColor:'white',
    shadowColor:'#000',
    shadowOffset:{
        width:0,
        height:2,
    },
    shadowOpacity: 0.25,
    shadowRadius:3.84,
    elevation:5,
  },
  passData:{
    width:'100%',
    backgroundColor:'white',
    borderRadius:4,
    shadowColor:'#000',
    shadowOffset:{
        width:0,
        height:2,
    },
    shadowOpacity: 0.25,
    shadowRadius:3.84,
    elevation:5,
  }
})
export default SignUp;
   
