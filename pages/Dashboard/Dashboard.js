import AsyncStorage from '@react-native-community/async-storage';
import React from 'react';
import { View,Text,StyleSheet,Button} from 'react-native';

function Dashboard({navigation, route}) {    
  // const {name} = route.params;
  const clearStorage = async () => {
    try{
      await AsyncStorage.clear()
      alert('storage successfully cleared!');
    } catch(e){
      alert('Failed to clear Async storage');
    }
  }

  const logout = () => {
    clearStorage();
    navigation.navigate('SignUp');
  }
    return (
        <View style={styles.container}>
            <Text>{route.params.name}</Text>    
            <Button title="Logout" onPress={()=>logout()}/>   
        </View>
    )
  }
  
const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'salmon',
    justifyContent:'center',
    alignItems:'center',
  }
})

export default Dashboard;