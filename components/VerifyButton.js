import React from 'react'
import {TouchableHighlight,View,Text,StyleSheet} from 'react-native'

class VerifyButton extends React.Component {
    constructor(props){
        super(props)
    }
    render(){
        return (
            <View style={styles.container}>
             <TouchableHighlight 
                activeOpacity={0.6}
                onPress={this.props.onConfirm}> 
                <View style={styles.verify_btn_box}>
                <Text style={styles.btn_content}>Confirm</Text>
                </View>            
            </TouchableHighlight>   
            </View>
)}}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'wheat',
        justifyContent:'center',
        alignItems:'center',
    },
    verify_btn_box:{
        height:'50%',
        backgroundColor:'#0B4F6C',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:30,
        padding:20,
    },
    btn_content:{
        textTransform:'uppercase',
        fontSize:18,
        color:'white',
    },
})

export default VerifyButton;
