import React from 'react';
import SignUp from '../pages/SignUp/SignUp';
import Login from '../pages/Login/Login';
import Home from '../pages/Home/Home';
import MyAccount from '../components/MyAccount';
import Dashboard from '../pages/Dashboard/Dashboard';
import { createStackNavigator } from "@react-navigation/stack";
import { Button } from 'react-native';

const Stack = createStackNavigator();

const AuthStack = () => {
  return(
    <>
    <Stack.Navigator initialRouteName="SignUp">
      <Stack.Screen 
        name="SignUp" 
        component={SignUp} 
        options={{title:'Sign Up Page'}}/>
      <Stack.Screen 
        name="Login" 
        component={Login}/>
      <Stack.Screen 
        name="Home" 
        component={Home}/>
      <Stack.Screen 
        name="MyAccount" 
        component={MyAccount}/>
      <Stack.Screen 
        name="Dashboard" 
        component={Dashboard}/>
    </Stack.Navigator>
    </>
  )
}

export default AuthStack;