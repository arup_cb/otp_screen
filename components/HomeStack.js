import React from 'react';
import { createStackNavigator, HeaderTitle } from "@react-navigation/stack";
import Dashboard from '../pages/Dashboard/Dashboard';
import MyAccount from './MyAccount';
import { Button } from 'react-native';
import SignUp from '../pages/SignUp/SignUp';

const Stack = createStackNavigator();

const HomeStack = () => {
  return (
    <Stack.Navigator initialRouteName="MyAccount">
      <Stack.Screen 
        name="MyAccount" 
        component={MyAccount}/>
      <Stack.Screen 
        name="Dashboard" 
        component={Dashboard} 
        options={({route})=>({title:route.params.name})}/>
        <Stack.Screen 
        name="SignUp" 
        component={SignUp} />
    </Stack.Navigator>
  )
}

export default HomeStack;