import React from 'react';
import {View, Button, StyleSheet, TextInput} from 'react-native';
import { useRef, useState, useEffect, useCallback } from 'react'

function EnterField(props) {
    const maxCount = props.maxCount;
    const [boxCount, setBoxCount] = useState([]);
    const [userNumber, setUserNumber] = useState(); 

    const limitFunc = (maxCount) => {
        let arrayList = [];
        for (let i = 0; i < maxCount; i++) {
            arrayList.push({
                index:i,
                value:'',
                ref:React.createRef(),
            });
        }
        setBoxCount(arrayList);
    }
            
    useEffect(() => {
        limitFunc(maxCount);    
    }, [userNumber])

    const handleChange = (text,index) =>{
        let updatedBoxCount = [...boxCount];
        updatedBoxCount[index].value = text;
        setBoxCount(updatedBoxCount);
        if(boxCount[index].value !== ''){
                if(index+1 < maxCount){
                boxCount[index+1].ref.current.focus()
            }
        }
        if(boxCount){
            validateUserOtp()
        }
    };

    const validateUserOtp = () => {
        let newArray = [];
        for(let j=0;j<boxCount.length; j++){
            // const {value:numbers} = boxCount; can i use destructuring
            newArray.push(boxCount[j].value);
        }
        props.authUserData(newArray);
    };

    return (  
        <View style={styles.container}>
            {boxCount.map((item,index)=>{
                return <View style={styles.sub_container} key={index}> 
                    <View style={styles.container1}>
                        <View style={styles.input_box}>
                            <TextInput 
                                style={styles.entry_field}
                                onChangeText={(text)=>handleChange(text,index)}
                                defaultValue={boxCount[index].value}
                                ref={boxCount[index].ref}
                                keyboardType="numeric"
                                maxLength={1}>
                            </TextInput>  
                        </View>
                    </View>
                </View>
            })}             
        </View>  
    )
}

const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        flex:1,
   },
   sub_container:{
       flex:1,
       flexDirection:'row',
   },
   input_box:{
    width:'60%',
    },
    entry_field:{
    width:'100%',
    textAlign:'center',
    fontSize:25,
    borderBottomWidth:2,
    },
    container1:{
    backgroundColor:'wheat',
    flex:1,
    justifyContent:'center',
    alignItems:'center',
}
});

export default EnterField;
    
        


