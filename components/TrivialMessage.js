import React from 'react'

import {
    View,
    Text,
    StyleSheet
} from 'react-native'

function TrivialMessage() {
    return (
        <View style={styles.container}>
            <View style={styles.title_box}>
                <Text style={styles.title}>Please verify your OTP sent on your registered Email ID.</Text>
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    title_box:{
        flex:1,
        backgroundColor:'wheat',
        alignItems:'center',
        justifyContent:'center',
        
    },
    title:{
        color:'rgb(48, 59, 71)',
        fontSize: 30,
        display:'flex',
        textAlign:'center',
        margin:20,
    },
})

export default TrivialMessage