import React,{useEffect} from 'react'
import { View,Text,StyleSheet, Button} from 'react-native'
import Dashboard from '../pages/Dashboard/Dashboard';

function MyAccount({navigation}) {

const goToDash = () =>{
        navigation.navigate('Dashboard',{name:'Hello and Welcome'});
    }
    return (
        <View style={styles.container}>
            <Text>Go to Dashboard</Text>
            <Button title="Click Here" onPress={()=>goToDash()}/>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:'green',
      justifyContent:'center',
      alignItems:'center',
    }
})

export default MyAccount;