import React from 'react';
import { createDrawerNavigator } from "@react-navigation/drawer";
import Dashboard from '../pages/Dashboard/Dashboard';
import MyAccount from './MyAccount';
// import LogOut from './Logout';

const Drawer = createDrawerNavigator();

const ProfileDrawer = () => {
  return (
    <Drawer.Navigator initialRouteName="Dashboard">
      <Drawer.Screen name="Dashboard" component={Dashboard} options={{headerRight:()=>(<Button title="Click me" onPress={()=>alert('hello')}/>)}}/>
      <Drawer.Screen name="MyAccount" component={MyAccount}/>
      {/* <Drawer.Screen name="LogOut" component={LogOut}/> */}
    </Drawer.Navigator>
  )
}

export default ProfileDrawer;